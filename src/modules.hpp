int  product(int a, int b)
{
	int result = 0;
	int i = 0;
	
	if (b < 0)
	{
		a = -a;
		b = -b;
	}
	
	if (a == 0 || b == 0)
	{
		result = 0;
	}
	else 
	{
		for (i = 0; i < b; i++)
		{
			result += a;
		}
	}
	
	return result;

}
